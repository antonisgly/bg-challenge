export interface ResponseInterface {
    history: DailyNested;
}

export interface DailyNested {
    dailysummary: [ Values ];
}

export interface Values {
    maxhumidity: string;
    maxtempm: string;
    mintempm: string;
    precipm: string;
}

