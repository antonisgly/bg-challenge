import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'inputFilter'
})
export class InputFilterPipe implements PipeTransform {

  transform( cities: any, input: any): any {

      /** Check if search input is undefined **/
      if ( input === undefined ) { return cities; }

      /** Otherwise return the updated list - Return the cities starts with the input value **/
      return cities.filter( city => city.startsWith( input ) );

  }

}
