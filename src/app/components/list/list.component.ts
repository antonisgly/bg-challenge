import { Component, OnInit, Input } from '@angular/core';
import { InputFilterPipe } from '../../pipes/input-filter.pipe';
import { ForecastService } from '../../services/forecast/forecast.service';
import { HttpClient } from '@angular/common/http';
import { ResponseInterface } from '../../interfaces/responseInterface';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [ InputFilterPipe ]
})

export class ListComponent implements OnInit {

  /** Declare Cities Array **/
  cities = [ 'New York', 'San Francisco', 'Chicago', 'Los Angeles', 'Detroit' ];

  /** Declare an object who will grab the forecast object **/
  dailyForecast: any = {};

  /** Tell component for incoming inputs - User's input from parent component **/
  @Input() userInput;

  constructor( private forecast: ForecastService,  private http: HttpClient ) { }

  ngOnInit() { }

  /** Call the getForecast method from forecast service to subscribe the observer **/
  getWeather( city ) {
    this.forecast.getForecast( city ).subscribe( ( response: ResponseInterface ) => {

        /** Destructure response object and get specific values **/
        const { history: { dailysummary: [ forecast  ] } } = response;

        /** Check if response is empty **/
        const OpenModal = function( forecast ) {
            for ( const key in forecast ) {
               return forecast.hasOwnProperty( key );
            }
        };

        /** If it's true bind data **/
        if ( OpenModal(forecast) ) {
            this.dailyForecast = forecast;
        }

    }, error => this.dailyForecast = error);
  }

}


