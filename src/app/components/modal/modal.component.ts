import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  forecast: object = {};

  /** Tell component for incoming inputs and open modal on new value **/
  @Input() set forecastData( forecastData: object ) {
      this.forecast = forecastData;
  }

  constructor() { }

  ngOnInit() { }

}
