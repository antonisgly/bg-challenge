import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

/** Core Services **/
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

/** Services **/
import { ForecastService } from './services/forecast/forecast.service';

/** Components **/
import { AppComponent } from './components/app/app.component';
import { SearchComponent } from './components/search/search.component';
import { ListComponent } from './components/list/list.component';

/** Pipes **/
import { InputFilterPipe } from './pipes/input-filter.pipe';
import { ModalComponent } from './components/modal/modal.component';


@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    InputFilterPipe,
    ListComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpClientModule
  ],
  providers: [ ForecastService ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
