import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ForecastService {

  /** WUnderground API link **/
  apiURL = 'http://api.wunderground.com/api/9f916997dd286235/history_20171030/q';
  /** Contains city's endpoint for the API call **/
  cityEndpoint: string;

  constructor( private http: HttpClient ) { }

  getForecast( city ) {

      /** Check city's name and create the endpoint **/
      switch ( city ) {
          case 'New York': {
              this.cityEndpoint = `/NY/${ city.split(' ').join('_') }`;
              break;
          }
          case 'San Francisco': {
              this.cityEndpoint = `/CA/${ city.split(' ').join('_') }`;
              break;
          }
          case 'Chicago': {
              this.cityEndpoint = `/IL/${ city.split(' ').join('_') }`;
              break;
          }
          case 'Los Angeles': {
              this.cityEndpoint = `/CA/${ city.split(' ').join('_') }`;
              break;
          }
          case 'Detroit': {
              this.cityEndpoint = `/MI/${ city.split(' ').join('_') }`;
              break;
          }
          default: {
              this.cityEndpoint = null;
          }
      }

      /** Return the data from response **/
      return this.http.get(`${ this.apiURL }${ this.cityEndpoint }.json`);

  }

}
