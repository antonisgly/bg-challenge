# BgChallenge
***

### Previews

![Homepage](/previews/homepage.jpg)

![Modal](/previews/modal.png)


## Install depedencies
#### _Run __npm install__ to install depedencies_


## Run | Deploy
#### _Run __ng serve__ for run or __ng build__ for deploy_


## File Structure

    .
    ├── ...
    ├── src                   
	    ├── app          	  
    	    ├── components 	  # Component's folder
			├── interfaces	  # Typescript's interfaces
			├── pipes		  # Pipes / Filters
			├── services
				├── forecast  # Forecast's services
	    ├── assets    
			└── images        # Images - logos,etc
			└── scss          # SCSS - Styles
    	 		└── ...       # Base, Vendor, Vars, MQs, Vars, Funcs
				


   
